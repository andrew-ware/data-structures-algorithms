# data-structures-algorithms
Implementations of common Data Structures and Algorithms in JavaScript


*IN PROGRESS*
#### Data Structures
✅ Linked List  
✅ Doubly Linked List  
⚪️ Binary Tree  
⚪️ Binary Search Tree  
⚪ N-ary Tree  
✅ Trie  
✅ Array (Static Array)  
✅ Vector/ArrayList (Dynamic Array)  
✅ Hash Table  
✅️ Stack  
✅ Queue  
⚪️ Heap  

#### Algorithms
⚪️ Breadth-First Search  
⚪️ Depth-First Search  
✅ Binary Search  
✅ Merge Sort  
⚪️ Quick Sort  