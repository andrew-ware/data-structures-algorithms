// Complexity:
// Time: O(1)
// Space: O(1)
const isArrayEmpty = arr => {
  if (arr && arr.length) {
    return !arr.length;
  }
  return true;
};

module.exports = { isArrayEmpty };
