// statically allocated array
// acts like the Array type in most compiled languages (i.e. Java, C++)
class StaticArray {
  constructor(...args) {
    const [arg1 = 0, arg2] = args;
    this.initVal = arg2;
    if (typeof arg1 === 'number') {
      // this corresponds to calling the constructor with params size and [optional] initVal
      this.array = new Array(arg1);
      if (Array.isArray(arg2)) {
        for (let i = 0; i < this.array.length; i += 1) {
          this.array[i] = arg2.slice();
        }
      } else if (typeof arg2 === 'object' && arg2 !== null) {
        for (let i = 0; i < this.array.length; i += 1) {
          this.array[i] = Object.assign(
            Object.create(Object.getPrototypeOf(arg2)),
            arg2
          );
        }
      } else if (typeof arg2 !== 'undefined') {
        this.array.fill(arg2);
      }
    } else if (Array.isArray(arg1)) {
      // this corresponds to calling the constructor with an array to make a copy of
      this.array = arg1;
    } else {
      throw new TypeError('Illegal constructor param types');
    }
  }

  get length() {
    return this.array.length;
  }

  get(index) {
    if (typeof index !== 'number') {
      throw new TypeError('Illegal index type');
    } else if (!this.length || index < 0 || index >= this.length) {
      throw new RangeError('Index out of range');
    }
    return this.array[index];
  }

  set(index, value) {
    if (typeof index !== 'number') {
      throw new TypeError('Illegal index type');
    } else if (!this.length || index < 0 || index >= this.length) {
      throw new RangeError('Index out of range');
    }
    this.array[index] = value;
  }

  unset(index) {
    this.set(index, this.initVal);
  }
}

module.exports = StaticArray;
