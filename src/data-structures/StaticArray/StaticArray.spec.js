const sinon = require('sinon');
const StaticArray = require('./StaticArray');

describe('Static Array', () => {
  describe('Constructor', () => {
    it('Initializes an Array with the provided size', () => {
      const staticArr = new StaticArray(10);
      assert(staticArr.length === 10);
    });

    it('Initializes an Array with empty elements if no provided initVal', () => {
      const staticArr = new StaticArray(1);
      assert(typeof staticArr.array[0] === 'undefined');
    });

    describe('Initializes an Array with provided initVal', () => {
      it('Provided initVal is a number', () => {
        const size = 10;
        const initVal = 0;
        const staticArr = new StaticArray(size, initVal);
        for (let i = 0; i < size; i += 1) {
          assert(staticArr.array[i] === initVal);
        }
      });

      it('Provided initVal is a string', () => {
        const size = 10;
        const initVal = '0';
        const staticArr = new StaticArray(size, initVal);
        for (let i = 0; i < size; i += 1) {
          assert(staticArr.array[i] === initVal);
        }
      });

      it('Provided initVal is another Array', () => {
        const initVal = [1, 2, 3];
        const staticArr = new StaticArray(initVal);
        for (let i = 0; i < initVal.length; i += 1) {
          assert(staticArr.array[i] === initVal[i]);
        }
      });

      it('Provided initVal is another Array along with a size', () => {
        const size = 10;
        const initVal = [1, 2, 3];
        const staticArr = new StaticArray(10, initVal);
        for (let i = 0; i < size; i += 1) {
          for (let j = 0; j < initVal.length; j += 1) {
            assert(staticArr.array[i][j] === initVal[j]);
          }
        }
      });

      it('Provided initVal is an object', () => {
        const size = 10;
        const key = 'foo';
        const val = 'bar';
        const initVal = { [key]: val };
        const staticArr = new StaticArray(size, initVal);
        for (let i = 0; i < size; i += 1) {
          assert(staticArr.array[i][key] === val);
        }
      });
    });

    describe('Throws TypeError if provided first param is not valid', () => {
      it('Provided first param is a plain object', () => {
        expect(() => new StaticArray({ 0: 1 })).to.throw(TypeError);
      });

      it('Provided first param is a Function', () => {
        expect(() => new StaticArray(() => {})).to.throw(TypeError);
      });

      it('Provided first param is a Date', () => {
        expect(() => new StaticArray(new Date())).to.throw(TypeError);
      });

      it('Provided first param is a string', () => {
        expect(() => new StaticArray('1')).to.throw(TypeError);
      });
    });
  });

  describe('Get', () => {
    it('Throws TypeError if provided index is not a number', () => {
      const staticArr = new StaticArray(1);
      expect(() => staticArr.get('1')).to.throw(TypeError);
    });

    describe('Throws error if attempt to get index that is out of range', () => {
      it('index < 0', () => {
        const staticArr = new StaticArray(1);
        expect(() => staticArr.get(-1)).to.throw(RangeError);
      });

      it('array empty', () => {
        const staticArr = new StaticArray();
        expect(() => staticArr.get(0)).to.throw(RangeError);
      });

      it('index > array.size', () => {
        const staticArr = new StaticArray(1);
        expect(() => staticArr.get(1)).to.throw(RangeError);
      });
    });

    it('Returns element at provided index', () => {
      const initVal = [1, 2, 3];
      const staticArr = new StaticArray(initVal);
      assert(staticArr.get(1) === initVal[1]);
    });
  });

  describe('Set', () => {
    it('Throws TypeError if provided index is not a number', () => {
      const staticArr = new StaticArray(1);
      expect(() => staticArr.set('1')).to.throw(TypeError);
    });

    describe('Throws error if attempt to get index that is out of range', () => {
      it('index < 0', () => {
        const staticArr = new StaticArray(1);
        expect(() => staticArr.set(-1)).to.throw(RangeError);
      });

      it('array empty', () => {
        const staticArr = new StaticArray();
        expect(() => staticArr.set(0)).to.throw(RangeError);
      });

      it('index > array.size', () => {
        const staticArr = new StaticArray(1);
        expect(() => staticArr.set(1)).to.throw(RangeError);
      });
    });

    it('Sets element at provided index with provided value', () => {
      const initVal = [1, 2, 3];
      const staticArr = new StaticArray(initVal);
      staticArr.set(1, 1);
      assert(staticArr.array[1] === 1);
    });
  });

  describe('Unset', () => {
    it('Calls set with provided index and initVal', () => {
      const setSpy = sinon.spy(StaticArray.prototype, 'set');
      const initVal = -1;
      const staticArr = new StaticArray(10, initVal);
      for (let i = 0; i < staticArr.length; i += 1) {
        staticArr.array[i] = i;
      }
      const removeIdx = 1;
      staticArr.unset(removeIdx);
      assert(setSpy.called);
      assert(setSpy.firstCall.args[0] === removeIdx);
      assert(setSpy.firstCall.args[1] === initVal);
    });
  });
});
