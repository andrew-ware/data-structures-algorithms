const DynamicArray = require('../DynamicArray');

class Stack {
  constructor() {
    this.data = new DynamicArray();
  }

  isEmpty() {
    return !this.data.length;
  }

  push(x) {
    this.data.add(x);
  }

  pop() {
    if (this.isEmpty()) {
      throw new Error('Cannot pop from empty stack');
    }
    const top = this.top();
    this.data.remove(this.data.length - 1);
    return top;
  }

  top() {
    if (this.isEmpty()) {
      throw new Error('Cannot get top of empty stack');
    }
    return this.data.get(this.data.length - 1);
  }
}

module.exports = Stack;
