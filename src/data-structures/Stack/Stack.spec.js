const Stack = require('./Stack');

describe('Stack', () => {
  describe('#push', () => {
    it('adds new element to stack', () => {
      const stack = new Stack();
      stack.push(1);
      stack.push(2);
      assert(stack.data.get(0) === 1);
      assert(stack.data.get(1) === 2);
    });
  });

  describe('#pop', () => {
    it('throws error if stack is empty', () => {
      const stack = new Stack();
      expect(stack.pop).to.throw(Error);
    });

    it('pops top element and returns it', () => {
      const stack = new Stack();
      stack.push(1);
      stack.push(2);
      assert(stack.pop() === 2);
      assert(stack.pop() === 1);
    });
  });

  describe('#top', () => {
    it('throws error if stack is empty', () => {
      const stack = new Stack();
      expect(stack.top).to.throw(Error);
    });

    it('returns top element', () => {
      const stack = new Stack();
      stack.push(1);
      stack.push(2);
      assert(stack.top() === 2);
    });
  });

  describe('#isEmpty', () => {
    it('returns true if stack is empty', () => {
      const stack = new Stack();
      assert(stack.isEmpty());
    });

    it('returns false if stack is not empty', () => {
      const stack = new Stack();
      stack.push(1);
      assert(!stack.isEmpty());
    });
  });
});
