const LinkedList = require('../LinkedList');
const DoublyLinkedListNode = require('./DoublyLinkedListNode');

class DoublyLinkedList extends LinkedList {
  // Complexity:
  // Time: O(1)
  // Space: O(1)
  addAtHead(node) {
    if (!(node instanceof DoublyLinkedListNode)) {
      throw new TypeError('Illegal node type');
    }
    node.prev = null;
    super.addAtHead(node);
  }

  // Complexity:
  // Time: O(n)
  // Space: O(1)
  addAtTail(node) {
    if (!(node instanceof DoublyLinkedListNode)) {
      throw new TypeError('Illegal node type');
    } else if (!this.head) {
      this.addAtHead(node);
      return;
    }
    const tailNode = this.getTailNode();
    tailNode.next = node;
    node.prev = tailNode;
  }

  // Complexity:
  // Time: O(n)
  // Space: O(1)
  addAtIndex(index, val) {
    if (typeof index !== 'number') {
      throw new TypeError('Illegal index type');
    } else if (index < 0) {
      throw new RangeError('Index out of range');
    } else if (index === 0) {
      const node = new DoublyLinkedListNode(val);
      this.addAtHead(node);
    } else {
      let i = 1;
      let curr = this.head;
      while (i < index && curr) {
        i += 1;
        curr = curr.next;
      }
      if (!curr || i !== index) {
        throw new RangeError('Index out of range');
      }
      const node = new DoublyLinkedListNode(val);
      curr.next.prev = node;
      node.next = curr.next;
      curr.next = node;
      node.prev = curr;
    }
  }

  // Complexity:
  // Time: O(n)
  // Space: O(1)
  deleteAtIndex(index) {
    if (typeof index !== 'number') {
      throw new TypeError('Illegal index type');
    } else if (index < 0 || !this.head) {
      throw new RangeError('Index out of range');
    } else if (index === 0) {
      this.head = this.head.next;
      this.head.prev = null;
    } else {
      // one or both of the following getNode calls will throw a RangeError if illegal index
      const prev = this.getNode(index - 1);
      const curr = this.getNode(index);
      prev.next = curr.next;
      curr.next.prev = prev;
    }
  }
}

module.exports = DoublyLinkedList;
