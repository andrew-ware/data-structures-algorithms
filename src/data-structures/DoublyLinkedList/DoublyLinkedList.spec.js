const sinon = require('sinon');
const LinkedList = require('../LinkedList');
const DoublyLinkedList = require('./DoublyLinkedList');
const DoublyLinkedListNode = require('./DoublyLinkedListNode');

describe('Doubly Linked List', () => {
  describe('Constructor', () => {
    it('Is instance of LinkedList', () => {
      const doublyLinkedList = new DoublyLinkedList();
      assert(doublyLinkedList instanceof LinkedList);
    });
  });

  describe('Add At Head', () => {
    it('Throws TypeError if provided node is not a DoublyLinkedListNode', () => {
      const doublyLinkedList = new DoublyLinkedList();
      expect(() => doublyLinkedList.addAtHead({})).to.throw(TypeError);
    });

    it('Calls super.addAtHead', () => {
      const addAtHeadSpy = sinon.spy(LinkedList.prototype, 'addAtHead');
      const doublyLinkedList = new DoublyLinkedList();
      const node = new DoublyLinkedListNode(1);
      doublyLinkedList.addAtHead(node);
      assert(addAtHeadSpy.called);
      addAtHeadSpy.restore();
    });
  });

  describe('Add At Tail', () => {
    it('Throws TypeError if provided node is not a DoublyLinkedListNode', () => {
      const doublyLinkedList = new DoublyLinkedList();
      expect(() => doublyLinkedList.addAtTail({})).to.throw(TypeError);
    });

    it('Passes node to addAtHead if null head', () => {
      const addAtHeadSpy = sinon.spy(DoublyLinkedList.prototype, 'addAtHead');
      const doublyLinkedList = new DoublyLinkedList();
      const tailVal = 1;
      const tail = new DoublyLinkedListNode(tailVal);
      doublyLinkedList.addAtTail(tail);
      assert(addAtHeadSpy.called);
      assert(addAtHeadSpy.firstCall.args[0]);
      assert(addAtHeadSpy.firstCall.args[0].val === tailVal);
    });

    it('Sets last node next to provided node', () => {
      const doublyLinkedList = new DoublyLinkedList();
      const headVal = 1;
      const head = new DoublyLinkedListNode(headVal);
      const tailVal = 2;
      const tail = new DoublyLinkedListNode(tailVal);
      doublyLinkedList.head = head;
      doublyLinkedList.addAtTail(tail);
      assert(doublyLinkedList.head !== null);
      assert(doublyLinkedList.head.next !== null);
      assert(doublyLinkedList.head.next.val === tailVal);
    });

    it('Sets provided node prev to curr tail node', () => {
      const doublyLinkedList = new DoublyLinkedList();
      const headVal = 1;
      const head = new DoublyLinkedListNode(headVal);
      const tailVal = 2;
      const tail = new DoublyLinkedListNode(tailVal);
      doublyLinkedList.head = head;
      doublyLinkedList.addAtTail(tail);
      assert(doublyLinkedList.head !== null);
      assert(doublyLinkedList.head.next !== null);
      assert(doublyLinkedList.head.next.prev !== null);
      assert(doublyLinkedList.head.next.prev.val === headVal);
    });
  });

  describe('Add At Index', () => {
    it('Throws TypeError if provided index is not a number', () => {
      const doublyLinkedList = new DoublyLinkedList();
      expect(() => doublyLinkedList.addAtIndex('1', 1)).to.throw(TypeError);
    });

    it('Adds at head if index 0', () => {
      const doublyLinkedList = new DoublyLinkedList();
      const nodeVal = 1;
      doublyLinkedList.addAtIndex(0, nodeVal);
      assert(doublyLinkedList.head !== null);
      assert(doublyLinkedList.head.val === nodeVal);
    });

    describe('Throws error if attempt to add into index that is out of range', () => {
      it('index < 0', () => {
        const doublyLinkedList = new DoublyLinkedList();
        expect(() => doublyLinkedList.addAtIndex(-1, 1)).to.throw(RangeError);
      });

      it('index > list.size', () => {
        const doublyLinkedList = new DoublyLinkedList();
        expect(() => doublyLinkedList.addAtIndex(1, 1)).to.throw(RangeError);
      });
    });

    it('Adds new node with provided value at index', () => {
      const doublyLinkedList = new DoublyLinkedList();
      const firstNodeVal = 1;
      const firstNode = new DoublyLinkedListNode(firstNodeVal);
      const secondNodeVal = 2;
      const secondNode = new DoublyLinkedListNode(secondNodeVal);
      doublyLinkedList.head = firstNode;
      doublyLinkedList.head.next = secondNode;
      const addVal = 3;
      doublyLinkedList.addAtIndex(1, addVal);
      assert(doublyLinkedList.head !== null);
      assert(doublyLinkedList.head.next !== null);
      assert(doublyLinkedList.head.next.val === addVal);
    });

    it('Adjusts new node, existing nodes next, prev pointers appropriately', () => {
      const doublyLinkedList = new DoublyLinkedList();
      const firstNodeVal = 1;
      const firstNode = new DoublyLinkedListNode(firstNodeVal);
      const secondNodeVal = 2;
      const secondNode = new DoublyLinkedListNode(secondNodeVal);
      doublyLinkedList.head = firstNode;
      doublyLinkedList.head.next = secondNode;
      const addVal = 3;
      doublyLinkedList.addAtIndex(1, addVal);
      assert(doublyLinkedList.head !== null);
      assert(doublyLinkedList.head.next !== null);
      assert(doublyLinkedList.head.next.val === addVal);
      assert(doublyLinkedList.head.next.prev !== null);
      assert(doublyLinkedList.head.next.prev.val === firstNodeVal);
      assert(doublyLinkedList.head.next.next !== null);
      assert(doublyLinkedList.head.next.next.val === secondNodeVal);
      assert(doublyLinkedList.head.next.next.prev !== null);
      assert(doublyLinkedList.head.next.next.prev.val === addVal);
    });
  });

  describe('Delete At Index', () => {
    it('Throws TypeError if provided index is not a number', () => {
      const doublyLinkedList = new DoublyLinkedList();
      expect(() => doublyLinkedList.deleteAtIndex('1', 1)).to.throw(TypeError);
    });

    describe('Throws error if attempt to add into index that is out of range', () => {
      it('index < 0', () => {
        const doublyLinkedList = new DoublyLinkedList();
        expect(() => doublyLinkedList.deleteAtIndex(-1)).to.throw(RangeError);
      });

      it('empty list', () => {
        const doublyLinkedList = new DoublyLinkedList();
        expect(() => doublyLinkedList.deleteAtIndex(1)).to.throw(RangeError);
      });

      it('index > list.size', () => {
        const doublyLinkedList = new DoublyLinkedList();
        expect(() => doublyLinkedList.deleteAtIndex(1)).to.throw(RangeError);
      });
    });

    it('If index is 0, updates head to current head next, sets head prev null', () => {
      const doublyLinkedList = new DoublyLinkedList();
      const headVal = 1;
      const head = new DoublyLinkedListNode(headVal);
      const tailVal = 2;
      const tail = new DoublyLinkedListNode(tailVal);
      doublyLinkedList.head = head;
      doublyLinkedList.head.next = tail;
      doublyLinkedList.deleteAtIndex(0);
      assert(doublyLinkedList.head !== null);
      assert(doublyLinkedList.head.val === tailVal);
      assert(doublyLinkedList.head.prev === null);
    });

    it('Removes node at provided index from doubly linked list', () => {
      const doublyLinkedList = new DoublyLinkedList();
      const firstVal = 1;
      const first = new DoublyLinkedListNode(firstVal);
      const secondVal = 2;
      const second = new DoublyLinkedListNode(secondVal);
      const thirdVal = 3;
      const third = new DoublyLinkedListNode(thirdVal);
      doublyLinkedList.head = first;
      doublyLinkedList.head.next = second;
      doublyLinkedList.head.next.next = third;
      doublyLinkedList.deleteAtIndex(1);
      assert(doublyLinkedList.head !== null);
      assert(doublyLinkedList.head.val === firstVal);
      assert(doublyLinkedList.head.next !== null);
      assert(doublyLinkedList.head.next.val === thirdVal);
    });

    it('Adjusts left, right nodes next, prev pointers appropriately', () => {
      const doublyLinkedList = new DoublyLinkedList();
      const firstVal = 1;
      const first = new DoublyLinkedListNode(firstVal);
      const secondVal = 2;
      const second = new DoublyLinkedListNode(secondVal);
      const thirdVal = 3;
      const third = new DoublyLinkedListNode(thirdVal);
      doublyLinkedList.head = first;
      doublyLinkedList.head.next = second;
      doublyLinkedList.head.next.next = third;
      doublyLinkedList.deleteAtIndex(1);
      assert(doublyLinkedList.head !== null);
      assert(doublyLinkedList.head.val === firstVal);
      assert(doublyLinkedList.head.next !== null);
      assert(doublyLinkedList.head.next.val === thirdVal);
      assert(doublyLinkedList.head.next.prev !== null);
      assert(doublyLinkedList.head.next.prev.val === firstVal);
    });
  });
});
