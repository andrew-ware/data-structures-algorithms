const LinkedListNode = require('../LinkedList/LinkedListNode');

class DoublyLinkedListNode extends LinkedListNode {
  constructor(val) {
    super(val);
    this.prev = null;
  }
}

module.exports = DoublyLinkedListNode;
