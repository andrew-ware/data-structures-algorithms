const LinkedListNode = require('../LinkedList/LinkedListNode');
const DoublyLinkedListNode = require('./DoublyLinkedListNode');

describe('Doubly Linked List Node', () => {
  describe('Constructor', () => {
    it('Is instance of LinkedListNode', () => {
      const node = new DoublyLinkedListNode();
      assert(node instanceof LinkedListNode);
    });

    it('Initializes with null prev', () => {
      const node = new DoublyLinkedListNode();
      assert(node.prev === null);
    });
  });
});
