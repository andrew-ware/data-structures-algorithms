const LinkedList = require('./LinkedList');
const LinkedListNode = require('./LinkedListNode');

describe('LinkedList', () => {
  describe('Constructor', () => {
    it('Initializes with null head', () => {
      const linkedList = new LinkedList();
      assert(linkedList.head === null);
    });
  });

  describe('Get Tail Node', () => {
    it('Returns null if null head', () => {
      const linkedList = new LinkedList();
      assert(linkedList.getTailNode() === null);
    });

    it('Returns last node in linked list', () => {
      const linkedList = new LinkedList();
      const headVal = 1;
      const head = new LinkedListNode(headVal);
      const tailVal = 2;
      const tail = new LinkedListNode(tailVal);
      linkedList.head = head;
      linkedList.head.next = tail;
      const tailNode = linkedList.getTailNode();
      assert(tailNode !== null);
      assert(tailNode.val === tailVal);
    });
  });

  describe('Add At Tail', () => {
    it('Throws TypeError if provided node is not a LinkedListNode', () => {
      const linkedList = new LinkedList();
      expect(() => linkedList.addAtTail({})).to.throw(TypeError);
    });

    it('Sets head if null head', () => {
      const linkedList = new LinkedList();
      const tailVal = 1;
      const tail = new LinkedListNode(tailVal);
      linkedList.addAtTail(tail);
      assert(linkedList.head !== null);
      assert(linkedList.head.val === tailVal);
    });

    it('Sets last node next to provided node', () => {
      const linkedList = new LinkedList();
      const headVal = 1;
      const head = new LinkedListNode(headVal);
      const tailVal = 2;
      const tail = new LinkedListNode(tailVal);
      linkedList.head = head;
      linkedList.addAtTail(tail);
      assert(linkedList.head !== null);
      assert(linkedList.head.next !== null);
      assert(linkedList.head.next.val === tailVal);
    });
  });

  describe('Add At Head', () => {
    it('Throws TypeError if provided node is not a LinkedListNode', () => {
      const linkedList = new LinkedList();
      expect(() => linkedList.addAtHead({})).to.throw(TypeError);
    });

    it('Sets node next to current head and updates head', () => {
      const linkedList = new LinkedList();
      const firstHeadVal = 1;
      const firstHead = new LinkedListNode(firstHeadVal);
      const secondHeadVal = 2;
      const secondHead = new LinkedListNode(secondHeadVal);
      linkedList.head = firstHead;
      linkedList.addAtHead(secondHead);
      assert(linkedList.head !== null);
      assert(linkedList.head.val === secondHeadVal);
      assert(linkedList.head.next !== null);
      assert(linkedList.head.next.val === firstHeadVal);
    });
  });

  describe('Get At Index', () => {
    it('Throws TypeError if provided index is not a number', () => {
      const linkedList = new LinkedList();
      expect(() => linkedList.addAtIndex('1', 1)).to.throw(TypeError);
    });

    describe('Throws error if attempt to add into index that is out of range', () => {
      it('index < 0', () => {
        const linkedList = new LinkedList();
        expect(() => linkedList.get(-1)).to.throw(RangeError);
      });

      it('list empty', () => {
        const linkedList = new LinkedList();
        expect(() => linkedList.get(0)).to.throw(RangeError);
      });

      it('index > list.size', () => {
        const linkedList = new LinkedList();
        const head = new LinkedListNode(1);
        linkedList.addAtHead(head);
        expect(() => linkedList.get(1)).to.throw(RangeError);
      });
    });

    it('Returns value of node at provided index', () => {
      const linkedList = new LinkedList();
      const firstNodeVal = 1;
      const firstNode = new LinkedListNode(firstNodeVal);
      const secondNodeVal = 2;
      const secondNode = new LinkedListNode(secondNodeVal);
      linkedList.head = firstNode;
      linkedList.head.next = secondNode;
      assert(linkedList.get(1) === secondNodeVal);
    });
  });

  describe('Add At Index', () => {
    it('Throws TypeError if provided index is not a number', () => {
      const linkedList = new LinkedList();
      expect(() => linkedList.addAtIndex('1', 1)).to.throw(TypeError);
    });

    it('Adds at head if index 0', () => {
      const linkedList = new LinkedList();
      const nodeVal = 1;
      linkedList.addAtIndex(0, nodeVal);
      assert(linkedList.head !== null);
      assert(linkedList.head.val === nodeVal);
    });

    describe('Throws error if attempt to add into index that is out of range', () => {
      it('index < 0', () => {
        const linkedList = new LinkedList();
        expect(() => linkedList.addAtIndex(-1, 1)).to.throw(RangeError);
      });

      it('index > list.size', () => {
        const linkedList = new LinkedList();
        expect(() => linkedList.addAtIndex(1, 1)).to.throw(RangeError);
      });
    });

    it('Adds new node with provided number value at index', () => {
      const linkedList = new LinkedList();
      const firstNodeVal = 1;
      const firstNode = new LinkedListNode(firstNodeVal);
      const secondNodeVal = 2;
      const secondNode = new LinkedListNode(secondNodeVal);
      linkedList.head = firstNode;
      linkedList.head.next = secondNode;
      const addVal = 3;
      linkedList.addAtIndex(1, addVal);
      assert(linkedList.head !== null);
      assert(linkedList.head.next !== null);
      assert(linkedList.head.next.val === addVal);
    });

    it('Adds new node with provided node value at index', () => {
      const linkedList = new LinkedList();
      const firstNodeVal = 1;
      const firstNode = new LinkedListNode(firstNodeVal);
      const secondNodeVal = 2;
      const secondNode = new LinkedListNode(secondNodeVal);
      linkedList.head = firstNode;
      linkedList.head.next = secondNode;
      const addVal = 3;
      const thirdNode = new LinkedListNode(addVal);
      linkedList.addAtIndex(1, thirdNode);
      assert(linkedList.head !== null);
      assert(linkedList.head.next !== null);
      assert(linkedList.head.next.val === addVal);
    });
  });

  describe('Delete At Index', () => {
    it('Throws TypeError if provided index is not a number', () => {
      const linkedList = new LinkedList();
      expect(() => linkedList.deleteAtIndex('1', 1)).to.throw(TypeError);
    });

    describe('Throws error if attempt to add into index that is out of range', () => {
      it('index < 0', () => {
        const linkedList = new LinkedList();
        expect(() => linkedList.deleteAtIndex(-1)).to.throw(RangeError);
      });

      it('empty list', () => {
        const linkedList = new LinkedList();
        expect(() => linkedList.deleteAtIndex(1)).to.throw(RangeError);
      });

      it('index > list.size', () => {
        const linkedList = new LinkedList();
        expect(() => linkedList.deleteAtIndex(1)).to.throw(RangeError);
      });
    });

    it('If index is 0, updates head to current head next', () => {
      const linkedList = new LinkedList();
      const headVal = 1;
      const head = new LinkedListNode(headVal);
      const tailVal = 2;
      const tail = new LinkedListNode(tailVal);
      linkedList.head = head;
      linkedList.head.next = tail;
      linkedList.deleteAtIndex(0);
      assert(linkedList.head !== null);
      assert(linkedList.head.val === tailVal);
    });

    it('Removes node at provided index from linked list', () => {
      const linkedList = new LinkedList();
      const firstVal = 1;
      const first = new LinkedListNode(firstVal);
      const secondVal = 2;
      const second = new LinkedListNode(secondVal);
      const thirdVal = 3;
      const third = new LinkedListNode(thirdVal);
      linkedList.head = first;
      linkedList.head.next = second;
      linkedList.head.next.next = third;
      linkedList.deleteAtIndex(1);
      assert(linkedList.head !== null);
      assert(linkedList.head.val === firstVal);
      assert(linkedList.head.next !== null);
      assert(linkedList.head.next.val === thirdVal);
    });
  });
});
