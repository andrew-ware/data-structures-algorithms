class LinkedListNode {
  constructor(val) {
    this.next = null;
    this.val = val;
  }
}

module.exports = LinkedListNode;
