const LinkedListNode = require('./LinkedListNode');

class LinkedList {
  constructor() {
    this.head = null;
  }

  // Complexity:
  // Time: O(1)
  // Space: O(1)
  addAtHead(node) {
    if (!(node instanceof LinkedListNode)) {
      throw new TypeError('Illegal node type');
    }
    node.next = this.head;
    this.head = node;
  }

  // Complexity:
  // Time: O(n)
  // Space: O(1)
  getTailNode() {
    let curr = this.head;
    while (curr && curr.next) {
      curr = curr.next;
    }
    return curr;
  }

  // Complexity:
  // Time: O(n)
  // Space: O(1)
  addAtTail(node) {
    if (!(node instanceof LinkedListNode)) {
      throw new TypeError('Illegal node type');
    } else if (!this.head) {
      this.head = node;
      return;
    }
    const tailNode = this.getTailNode();
    tailNode.next = node;
  }

  // Complexity:
  // Time: O(n)
  // Space: O(1)
  getNode(index) {
    if (typeof index !== 'number') {
      throw new TypeError('Illegal index type');
    } else if (index < 0 || !this.head) {
      throw new RangeError('Index out of range');
    }
    let i = 0;
    let curr = this.head;
    while (curr && i < index) {
      i += 1;
      curr = curr.next;
    }
    if (!curr || i !== index) {
      throw new RangeError('Index out of range');
    }
    return curr;
  }

  // Complexity:
  // Time: O(n)
  // Space: O(1)
  get(index) {
    return this.getNode(index).val;
  }

  // Complexity:
  // Time: O(n)
  // Space: O(1)
  addAtIndex(index, val) {
    if (typeof index !== 'number') {
      throw new TypeError('Illegal index type');
    } else if (index < 0) {
      throw new RangeError('Index out of range');
    } else if (index === 0) {
      const node =
        val instanceof LinkedListNode ? val : new LinkedListNode(val);
      this.addAtHead(node);
    } else {
      let i = 1;
      let curr = this.head;
      while (i < index && curr) {
        i += 1;
        curr = curr.next;
      }
      if (!curr || i !== index) {
        throw new RangeError('Index out of range');
      }
      const node =
        val instanceof LinkedListNode ? val : new LinkedListNode(val);
      node.next = curr.next;
      curr.next = node;
    }
  }

  // Complexity:
  // Time: O(n)
  // Space: O(1)
  deleteAtIndex(index) {
    if (typeof index !== 'number') {
      throw new TypeError('Illegal index type');
    } else if (index < 0 || !this.head) {
      throw new RangeError('Index out of range');
    } else if (index === 0) {
      this.head = this.head.next;
    } else {
      // one or both of the following getNode calls will throw a RangeError if illegal index
      const prev = this.getNode(index - 1); // O(n)
      const curr = prev.next;
      if (!curr) {
        throw new RangeError('Index out of range');
      }
      prev.next = curr.next;
    }
  }
}

module.exports = LinkedList;
