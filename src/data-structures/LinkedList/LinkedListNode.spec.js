const LinkedListNode = require('./LinkedListNode');

describe('LinkedListNode', () => {
  describe('Constructor', () => {
    it('Initializes with null next', () => {
      const node = new LinkedListNode();
      assert(node.next === null);
    });

    it('Initializes with provided val', () => {
      const val = 1;
      const node = new LinkedListNode(val);
      assert(node.val === val);
    });
  });
});
