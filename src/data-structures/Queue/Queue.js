const StaticArray = require('../StaticArray');

// circular queue implementation
class Queue {
  constructor(size = 0) {
    if (typeof size !== 'number') {
      throw new TypeError(
        'Must provide a numeric size parameter for Queue initialization.'
      );
    }
    this.data = new StaticArray(size, null);
    this.size = size;
    this.head = -1;
    this.tail = -1;
  }

  isFull() {
    return (this.tail + 1) % this.size === this.head;
  }

  isEmpty() {
    return this.head === -1;
  }

  enQueue(el) {
    if (this.isFull()) {
      return false;
    }
    if (this.head === -1) {
      this.head = 0;
    }
    this.tail = (this.tail + 1) % this.size;
    this.data.set(this.tail, el);
    return true;
  }

  deQueue() {
    if (this.isEmpty()) {
      return false;
    }
    if (this.head === this.tail) {
      this.head = -1;
      this.tail = -1;
      return true;
    }
    this.head = (this.head + 1) % this.size;
    return true;
  }

  Rear() {
    if (this.isEmpty()) {
      return -1;
    }
    return this.data.get(this.tail);
  }

  Front() {
    if (this.isEmpty()) {
      return -1;
    }
    return this.data.get(this.head);
  }
}

module.exports = Queue;
