const Queue = require('./Queue');

describe('Queue', () => {
  describe('#enQueue', () => {
    it('inserts if not full', () => {
      const queue = new Queue(3);
      assert(queue.enQueue(1));
      assert(queue.enQueue(2));
      assert(queue.enQueue(3));
      assert(!queue.enQueue(4));
    });
  });

  describe('#deQueue', () => {
    it('removes if not empty', () => {
      const queue = new Queue(3);
      assert(queue.enQueue(1));
      assert(queue.enQueue(2));
      assert(queue.deQueue());
      assert(queue.deQueue());
      assert(!queue.deQueue());
    });
  });

  describe('#isFull', () => {
    it('returns true if queue is full', () => {
      const queue = new Queue(3);
      queue.enQueue(1);
      queue.enQueue(2);
      queue.enQueue(3);
      assert(queue.isFull());
    });

    it('returns false if queue is not full', () => {
      const queue = new Queue(3);
      assert(!queue.isFull());
    });
  });

  describe('#isEmpty', () => {
    it('returns true if queue is empty', () => {
      const queue = new Queue(3);
      assert(queue.isEmpty());
    });

    it('returns false if queue is not empty', () => {
      const queue = new Queue(3);
      queue.enQueue(1);
      assert(!queue.isEmpty());
    });
  });

  describe('#Rear', () => {
    it('returns element at tail if not empty', () => {
      const queue = new Queue(3);
      queue.enQueue(1);
      queue.enQueue(2);
      queue.enQueue(3);
      queue.deQueue();
      queue.deQueue();
      queue.deQueue();
      queue.enQueue(4);
      queue.enQueue(5);
      assert(queue.Rear() === 5);
    });

    it('retruns -1 if empty', () => {
      const queue = new Queue(3);
      assert(queue.Rear() === -1);
    });
  });

  describe('#Front', () => {
    it('returns element at head', () => {
      const queue = new Queue(3);
      queue.enQueue(1);
      queue.enQueue(2);
      queue.enQueue(3);
      queue.deQueue();
      queue.deQueue();
      queue.deQueue();
      queue.enQueue(4);
      assert(queue.Front() === 4);
    });

    it('retruns -1 if empty', () => {
      const queue = new Queue(3);
      assert(queue.Front() === -1);
    });
  });
});
