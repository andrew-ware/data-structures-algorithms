class TrieNode {
  constructor() {
    this.children = {};
    this.word = null;
  }
}

class Trie {
  constructor() {
    this.root = new TrieNode();
  }

  insert(word) {
    let curr = this.root;
    word.split('').forEach(char => {
      if (typeof curr.children[char] === 'undefined') {
        curr.children[char] = new TrieNode(char);
      }
      curr = curr.children[char];
    });
    curr.word = word;
  }

  search(word) {
    const tail = this.getPrefixTail(word);
    return tail !== null && tail.word;
  }

  // return tail node for a given prefix or null if prefix not in trie
  getPrefixTail(prefix = '') {
    let curr = this.root;
    let idx = 0;
    while (curr && idx < prefix.length) {
      const next = curr.children[prefix[idx]];
      if (next) {
        curr = next;
      } else {
        curr = null;
      }
      idx += 1;
    }
    return curr;
  }

  startsWith(prefix) {
    return this.getPrefixTail(prefix) !== null;
  }
}

module.exports = Trie;
