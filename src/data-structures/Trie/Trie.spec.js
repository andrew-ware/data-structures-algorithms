const Trie = require('./Trie');

describe('Trie', () => {
  describe('#insert', () => {
    it('inserts new word', () => {
      const trie = new Trie();
      const word = 'word';
      trie.insert(word);
      let curr = trie.root;
      word.split('').forEach(char => {
        if (typeof curr.children[char] === 'undefined') {
          assert(false);
        }
        curr = curr.children[char];
      });
      assert(curr.word === word);
    });
  });

  describe('#startsWith', () => {
    it('returns false if prefix not in trie', () => {
      const trie = new Trie();
      assert(!trie.startsWith('dog'));
    });

    it('returns true if prefix in trie', () => {
      const trie = new Trie();
      const word = 'doggy';
      trie.insert(word);
      assert(trie.startsWith('dog'));
    });
  });

  describe('#search', () => {
    it('returns false if word is not in trie', () => {
      const trie = new Trie();
      assert(!trie.search('dog'));
    });

    it('returns false if word is prefix but not word', () => {
      const trie = new Trie();
      const word = 'doggy';
      trie.insert(word);
      assert(!trie.search('dog'));
    });

    it('returns false if word is prefix but not word', () => {
      const trie = new Trie();
      const word = 'doggy';
      trie.insert(word);
      assert(trie.search(word));
    });
  });
});
