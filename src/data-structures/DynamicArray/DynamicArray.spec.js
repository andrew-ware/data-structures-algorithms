const StaticArray = require('../StaticArray');
const DynamicArray = require('./DynamicArray');

describe('Dynamic Array', () => {
  describe('Constructor', () => {
    it('Initializes array as StaticArray', () => {
      const dynamicArray = new DynamicArray();
      assert(dynamicArray.array instanceof StaticArray);
    });

    it('Initializes array with provided size', () => {
      const size = 10;
      const dynamicArray = new DynamicArray(size);
      assert(dynamicArray.array.length === size);
    });

    it('Initializes array with provided size and initVal', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      for (let i = 0; i < size; i += 1) {
        dynamicArray.array.get(i) === initVal;
      }
    });

    it('Initializes array as copy of provided initVal', () => {
      const initVal = [1, 2, 3];
      const dynamicArray = new DynamicArray(initVal);
      for (let i = 0; i < initVal.length; i += 1) {
        dynamicArray.array.get(i) === initVal[i];
      }
    });

    it('Initializes realSize as 0 if no initVal provided', () => {
      const size = 10;
      const dynamicArray = new DynamicArray(size);
      assert(dynamicArray.realSize === 0);
    });

    it('Initializes realSize as size if initVal provided', () => {
      const size = 10;
      const initVal = 0;
      const dynamicArray = new DynamicArray(size, initVal);
      assert(dynamicArray.realSize === size);
    });

    it('Initializes realSize as length of provided initVal array', () => {
      const initVal = [1, 2, 3];
      const dynamicArray = new DynamicArray(initVal);
      assert(dynamicArray.realSize === initVal.length);
    });
  });

  describe('Length', () => {
    it('Returns 0 if array empty', () => {
      const size = 10;
      const dynamicArray = new DynamicArray(size);
      assert(dynamicArray.length === 0);
    });

    it('Returns realSize if array non-empty', () => {
      const initVal = [1, 2, 3];
      const dynamicArray = new DynamicArray(initVal);
      assert(dynamicArray.length === initVal.length);
    });
  });

  describe('Resize', () => {
    it('creates new array with provided newSize and initializes with copy of old array', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      dynamicArray.resize(size * 2);
      assert(dynamicArray.array.length === size * 2);
      for (let i = 0; i < size * 2; i += 1) {
        if (i < size) {
          assert(dynamicArray.array.get(i) === initVal);
        } else {
          assert(typeof dynamicArray.array.get(i) === 'undefined');
        }
      }
    });
  });

  describe('Get', () => {
    it('Throws RangeError if index less than 0', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      expect(() => dynamicArray.get(-1)).to.throw(RangeError);
    });

    it('Throws RangeError if index >= array.length', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      expect(() => dynamicArray.get(size)).to.throw(RangeError);
    });

    it('Throws RangeError if index >= realSize', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      dynamicArray.resize(size * 2);
      expect(() => dynamicArray.get(size)).to.throw(RangeError);
    });

    it('Returns value at index', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      assert(dynamicArray.get(0) === initVal);
    });
  });

  describe('Set', () => {
    it('Throws RangeError if index less than 0', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      expect(() => dynamicArray.set(-1)).to.throw(RangeError);
    });

    it('Throws RangeError if index >= array.length', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      expect(() => dynamicArray.set(size)).to.throw(RangeError);
    });

    it('Throws RangeError if index >= realSize', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      dynamicArray.resize(size * 2);
      expect(() => dynamicArray.set(size)).to.throw(RangeError);
    });

    it('Sets value at index', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      const setIdx = 0;
      const setVal = 2;
      dynamicArray.set(0, 2);
      assert(dynamicArray.array.get(setIdx) === setVal);
    });
  });

  describe('Add', () => {
    it('Increases realSize', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      dynamicArray.add(1);
      assert(dynamicArray.realSize === size + 1);
    });

    it('Adds value to end of array', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      const newVal = 2;
      dynamicArray.add(newVal);
      const lastIdx = dynamicArray.realSize - 1;
      assert(dynamicArray.array.get(lastIdx) === newVal);
    });
  });

  describe('Remove', () => {
    it('Throws RangeError if index less than 0', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      expect(() => dynamicArray.remove(-1)).to.throw(RangeError);
    });

    it('Throws RangeError if index >= array.length', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      expect(() => dynamicArray.remove(size)).to.throw(RangeError);
    });

    it('Throws RangeError if index >= realSize', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      dynamicArray.resize(size * 2);
      expect(() => dynamicArray.remove(size)).to.throw(RangeError);
    });

    it('Decreases realSize', () => {
      const size = 10;
      const initVal = 1;
      const dynamicArray = new DynamicArray(size, initVal);
      dynamicArray.remove(0);
      assert(dynamicArray.realSize === size - 1);
    });

    it('Removes element at provided index', () => {
      const initVal = [1, 2, 3];
      const dynamicArray = new DynamicArray(initVal);
      const removeIdx = 1;
      dynamicArray.remove(removeIdx);
      for (let i = 0; i < initVal.length - 1; i += 1) {
        if (i < removeIdx) {
          assert(dynamicArray.array.get(i) === initVal[i]);
        } else if (i > removeIdx) {
          assert(dynamicArray.array.get(i - 1) === initVal[i]);
        }
      }
    });
  });
});
