const StaticArray = require('../StaticArray');

class DynamicArray {
  constructor(...args) {
    this.array = new StaticArray(...args);
    const [arg1, arg2] = args;
    this.realSize = 0;
    if (typeof arg1 === 'number') {
      if (typeof arg2 !== 'undefined') {
        // if initialized as a non-empty array
        this.realSize = arg1;
      }
    } else if (arg1 && arg1.length) {
      // if initialized as copy of another array: arg1
      this.realSize = arg1.length;
    }
  }

  get length() {
    return this.realSize;
  }

  copy(array1, array2) {
    for (let i = 0; i < this.realSize; i += 1) {
      array2.set(i, array1.get(i));
    }
  }

  resize(newSize, copyFn = this.copy.bind(this)) {
    // make new array
    const newArray = new StaticArray(newSize);
    // copy old array over to new one
    copyFn(this.array, newArray);
    this.array = newArray;
  }

  doubleSize() {
    this.resize(this.array.length * 2 || 1);
  }

  get(index) {
    if (index < 0 || index >= this.realSize) {
      throw new RangeError('Index out of range');
    }
    return this.array.get(index);
  }

  set(index, val) {
    if (index < 0 || index >= this.realSize) {
      throw new RangeError('Index out of range');
    }
    this.array.set(index, val);
  }

  add(val) {
    if (this.realSize === this.array.length) {
      // we need to create more space for new elements
      this.doubleSize();
    }
    this.array.set(this.realSize, val);
    this.realSize += 1;
  }

  remove(index) {
    if (index < 0 || index >= this.realSize) {
      throw new RangeError('Index out of range');
    }
    this.resize(this.array.length - 1, (array1, array2) => {
      for (let i = 0; i < this.realSize; i += 1) {
        if (i < index) {
          array2.set(i, array1.get(i));
        } else if (i > index) {
          array2.set(i - 1, array1.get(i));
        }
      }
    });
    this.realSize -= 1;
  }
}

module.exports = DynamicArray;
