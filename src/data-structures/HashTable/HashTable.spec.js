const sinon = require('sinon');
const DynamicArray = require('../DynamicArray');
const HashTableLinkedList = require('./HashTableLinkedList');
const HashTableNode = require('./HashTableNode');
const {
  default: HashTable,
  INIT_BUCKET_COUNT,
  FILL_THRESHOLD
} = require('./HashTable');
const { getHashCode } = require('./util');

describe('Hash Table', () => {
  describe('Constructor', () => {
    it('Initializes DynamicArray buckets of INIT_BUCKET_COUNT length', () => {
      const hashTable = new HashTable();
      assert(hashTable.buckets instanceof DynamicArray);
      assert(hashTable.buckets.length === INIT_BUCKET_COUNT);
    });

    it('Initializes with bucketCount and fills each bucket with a new HashTableLinkedList', () => {
      const hashTable = new HashTable();
      for (let i = 0; i < hashTable.bucketCount; i += 1) {
        assert(hashTable.buckets.get(i) instanceof HashTableLinkedList);
      }
    });

    it('Initializes with empty size', () => {
      const hashTable = new HashTable();
      assert(hashTable.size === 0);
    });
  });

  describe('Is Empty', () => {
    it('Returns true if size is 0', () => {
      const hashTable = new HashTable();
      assert(hashTable.isEmpty());
    });

    it('Returns false if size not 0', () => {
      const hashTable = new HashTable();
      hashTable.size = 1; // manually set size
      assert(!hashTable.isEmpty());
    });
  });

  describe('Resize', () => {
    it('Thows new TypeError if newSize not a number', () => {
      const hashTable = new HashTable();
      expect(() => hashTable.resize('1')).to.throw(TypeError);
    });

    it('Thows new TypeError if newSize is negative', () => {
      const hashTable = new HashTable();
      expect(() => hashTable.resize(-1)).to.throw(TypeError);
    });

    it('Thows new TypeError if newSize is 0', () => {
      const hashTable = new HashTable();
      expect(() => hashTable.resize(0)).to.throw(TypeError);
    });

    it('Sets new buckets length', () => {
      const hashTable = new HashTable();
      const newSize = 20;
      hashTable.resize(newSize);
      assert(hashTable.buckets.length === newSize);
    });

    it('Adds old nodes into new buckets', () => {
      const addSpy = sinon.spy(HashTable.prototype, 'add');
      const hashTable = new HashTable();
      const hashTableLL = new HashTableLinkedList();
      hashTableLL.head = new HashTableNode(1, 1);
      hashTable.buckets.set(0, hashTableLL); // manually insert a node before resizing
      hashTable.resize(20);
      assert(addSpy.called);
    });
  });

  describe('Get Bucket Index', () => {
    it('Throws TypeError if key is not an object', () => {
      const hashTable = new HashTable();
      expect(() => hashTable.getBucketIndex(undefined)).to.throw(TypeError);
    });

    it('returns index of key', () => {
      const hashTable = new HashTable();
      assert(
        hashTable.getBucketIndex(1) === getHashCode(1) % hashTable.bucketCount
      );
    });
  });

  describe('Get', () => {
    it('Returns null if key does not exist in hash table', () => {
      const hashTable = new HashTable();
      assert(hashTable.get(1) === null);
    });

    it('Returns null if key does not exist in hash table', () => {
      const hashTable = new HashTable();
      const nodeKey = 1;
      const nodeVal = 1;
      const node = new HashTableNode(nodeKey, nodeVal);
      const bucketIdx = hashTable.getBucketIndex(nodeKey);
      hashTable.buckets.set(bucketIdx, node); // manually insert node
      assert(hashTable.buckets.get(bucketIdx) !== null);
      assert(hashTable.buckets.get(bucketIdx).val === nodeVal);
    });
  });

  describe('Add', () => {
    it('Updates existing val if key already exists in hash table', () => {
      const hashTable = new HashTable();
      const key = 1;
      const val = 1;
      const hashTableLL = new HashTableLinkedList();
      hashTableLL.head = new HashTableNode(key, val);
      hashTable.buckets.set(hashTable.getBucketIndex(key), hashTableLL); // manually insert a node before resizing
      const newVal = 2;
      hashTable.add(key, newVal);
      assert(hashTable.buckets.get(hashTable.getBucketIndex(key)) !== null);
      assert(
        hashTable.buckets.get(hashTable.getBucketIndex(key)).head.val === newVal
      );
    });

    it('Adds new node with provided key and val', () => {
      const hashTable = new HashTable();
      const key = 1;
      const val = 1;
      hashTable.add(key, val);
      assert(hashTable.buckets.get(hashTable.getBucketIndex(key)) !== null);
      assert(
        hashTable.buckets.get(hashTable.getBucketIndex(key)).head.val === val
      );
    });

    it('Adds new node as head of bucket corresponding to key', () => {
      const hashTable = new HashTable();
      const bucketIdx = 0;
      const first = 1;
      const hashTableLL = new HashTableLinkedList();
      hashTableLL.head = new HashTableNode(first, first);
      hashTable.buckets.set(bucketIdx, hashTableLL); // manually insert a node before resizing
      const second = 2;
      hashTable.add(second, second);
      assert(hashTable.buckets.get(bucketIdx) !== null);
      assert(hashTable.buckets.get(bucketIdx).head.val === second);
      assert(hashTable.buckets.get(bucketIdx).head.next !== null);
      assert(hashTable.buckets.get(bucketIdx).head.next.val === first);
    });

    it('Doubles size of hash table if new size >= FILL_THRESHOLD', () => {
      const resizeSpy = sinon.spy(HashTable.prototype, 'resize');
      const hashTable = new HashTable();
      hashTable.size = FILL_THRESHOLD * hashTable.bucketCount;
      hashTable.add(1, 1);
      assert(resizeSpy.called);
      assert(resizeSpy.firstCall.args[0] === INIT_BUCKET_COUNT * 2);
    });
  });

  describe('Remove', () => {
    it('Removes node with provided key', () => {
      const hashTable = new HashTable();
      const hashTableLL = new HashTableLinkedList();
      const headVal = 1;
      hashTableLL.addAtTail(new HashTableNode(2, headVal)); // key 2 will map to index 0
      const tailVal = 2;
      hashTableLL.addAtTail(new HashTableNode(40, tailVal)); // key 40 will map to index 0
      hashTable.buckets.set(0, hashTableLL);
      hashTable.remove(2);
      assert(hashTable.buckets.get(0).head !== null);
      assert(hashTable.buckets.get(0).head.val === tailVal);
      assert(hashTable.buckets.get(0).head.next === null);
    });
  });
});
