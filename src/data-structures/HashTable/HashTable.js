const DynamicArray = require('../DynamicArray');
const HashTableLinkedList = require('./HashTableLinkedList');
const { getHashCode } = require('./util');

// arbitrary-ish bucket count init size, fill threshold
const INIT_BUCKET_COUNT = 10;
const FILL_THRESHOLD = 0.7;

class HashTable {
  // Complexity:
  // Time: O(1)
  // Space: O(INIT_BUCKET_COUNT)
  constructor() {
    this.bucketCount = INIT_BUCKET_COUNT;
    this.buckets = new DynamicArray(
      INIT_BUCKET_COUNT,
      new HashTableLinkedList()
    );
    this.size = 0;
  }

  isEmpty() {
    return !this.size;
  }

  // Complexity:
  // Time: O(bucketCount)
  // Space: O(newSize)
  resize(newSize) {
    if (typeof newSize !== 'number' || newSize <= 0) {
      throw new TypeError('Illegal newSize');
    }
    const oldBuckets = this.buckets;
    this.bucketCount = newSize;
    this.buckets = new DynamicArray(newSize, new HashTableLinkedList());
    this.size = 0;

    // O(bucketCount)
    for (let i = 0; i < oldBuckets.length; i += 1) {
      let { head } = oldBuckets.get(i);
      while (head) {
        this.add(head.key, head.val);
        head = head.next;
      }
    }
  }

  // Complexity:
  // Time: O(1)
  // Space: O(1)
  getBucketIndex(key) {
    if (typeof key === 'undefined') {
      throw new TypeError('Illegal key');
    }
    return getHashCode(key) % this.bucketCount;
  }

  // Complexity:
  // Time: O(MAX_BUCKET_SIZE ~= FILL_THRESHOLD * bucketCount) / Ω(1)
  // Space: O(1)
  get(key) {
    const bucketIdx = this.getBucketIndex(key);
    let head = this.buckets.get(bucketIdx);

    while (head) {
      if (head.key === key) {
        return head.val;
      }
      head = head.next;
    }

    return null; // key not found in this bucket
  }

  // Complexity:
  // Time: O(MAX_BUCKET_SIZE ~= FILL_THRESHOLD * bucketCount) / Ω(1)
  // Space: O(1)
  add(key, val) {
    const bucketIdx = this.getBucketIndex(key);

    // check if key already exists
    // overwrite prev val if it does
    let { head } = this.buckets.get(bucketIdx);
    while (head) {
      if (head.key === key) {
        head.val = val;
        return;
      }
      head = head.next;
    }

    this.size += 1; // increment size
    this.buckets.get(bucketIdx).addAtHead(key, val);

    if (this.size / this.buckets.length >= FILL_THRESHOLD) {
      // double size of hash table
      // this will happen as infrequently as a DynamicArray needs to double its size, so the time complexity is negligable in the long run
      this.resize(this.bucketCount * 2);
    }
  }

  // Complexity:
  // Time: O(MAX_BUCKET_SIZE ~= FILL_THRESHOLD * bucketCount) / Ω(1)
  // Space: O(1)
  remove(key) {
    const bucketIdx = this.getBucketIndex(key);

    const bucket = this.buckets.get(bucketIdx);
    let { head } = bucket;
    let prev = null;
    while (head) {
      if (head.key === key) {
        if (prev) {
          prev.next = head.next;
        } else {
          bucket.head = bucket.head.next;
        }
        return;
      }
      prev = head;
      head = head.next;
    }
    throw new Error('Key not found in this bucket');
  }
}

module.exports = {
  default: HashTable,
  INIT_BUCKET_COUNT,
  FILL_THRESHOLD
};
