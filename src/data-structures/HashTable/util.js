// This is a weak hash fn but for demonstration purposes it works fine.
// Java has a nice built-in Object.hashCode() func which is built using Java's built-in Object.equals() func.
// In JS, it's not as easy to come up with a good hashCode fn.
// We'll skip coming up with a good one for now, as it's outside the scope of this DS.
/* eslint-disable-next-line no-extend-native */
const getHashCode = obj => {
  let json;
  try {
    json = JSON.stringify(obj);
    if (typeof json === 'undefined') {
      throw new TypeError('Illegal obj');
    }
    if (json === '') {
      return 0;
    }
  } catch (err) {
    throw err;
  }
  let hash = 0;
  for (let i = 0; i < json.length; i += 1) {
    hash = (hash << 5) - hash + json.charCodeAt(i);
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

module.exports = { getHashCode };
