const LinkedListNode = require('../LinkedList/LinkedListNode');
const HashTableNode = require('./HashTableNode');

describe('Hash Table Node', () => {
  describe('Constructor', () => {
    it('Is instance of LinkedListNode', () => {
      const node = new HashTableNode();
      assert(node instanceof LinkedListNode);
    });

    it('Initializes with provided key', () => {
      const key = 1;
      const node = new HashTableNode(key);
      assert(node.key === key);
    });
  });
});
