const { getHashCode } = require('./util');

describe('Hash Table Util', () => {
  describe('#getHashCode', () => {
    it('Throws TypeError if provided obj cannot be stringified', () => {
      expect(() => getHashCode(undefined)).to.throw(TypeError);
    });

    it('Throws TypeError if supply circular structure', () => {
      const foo = {};
      foo.bar = foo;
      expect(() => getHashCode(foo)).to.throw(TypeError);
    });

    it('Returns hash code for provided input', () => {
      assert(getHashCode(1) === 49);
    });
  });
});
