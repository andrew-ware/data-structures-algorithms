const sinon = require('sinon');
const LinkedList = require('../LinkedList');
const HashTableNode = require('./HashTableNode');
const HashTableLinkedList = require('./HashTableLinkedList');

describe('Hash Table Linked List', () => {
  describe('Constructor', () => {
    it('Initializes as instance of LinkedList', () => {
      const hashTableLL = new HashTableLinkedList();
      assert(hashTableLL instanceof LinkedList);
    });
  });

  describe('Add At Head', () => {
    it('Calls super.addAtHead with new HashTableNode', () => {
      const addAtHeadSpy = sinon.spy(LinkedList.prototype, 'addAtHead');
      const hashTableLL = new HashTableLinkedList();
      const key = 1;
      const val = 1;
      hashTableLL.addAtHead(key, val);
      assert(addAtHeadSpy.called);
      assert(addAtHeadSpy.firstCall.args[0] instanceof HashTableNode);
      assert(addAtHeadSpy.firstCall.args[0] !== null);
      assert(addAtHeadSpy.firstCall.args[0].key === key);
      assert(addAtHeadSpy.firstCall.args[0].val === val);
      addAtHeadSpy.restore();
    });
  });
});
