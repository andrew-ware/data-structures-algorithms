const LinkedListNode = require('../LinkedList/LinkedListNode');

class HashTableNode extends LinkedListNode {
  constructor(key, val) {
    super(val);
    this.key = key;
  }
}

module.exports = HashTableNode;
