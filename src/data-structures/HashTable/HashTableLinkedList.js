const LinkedList = require('../LinkedList');
const HashTableNode = require('./HashTableNode');

class HashTableLinkedList extends LinkedList {
  // Complexity:
  // Time: O(1)
  // Space: O(1)
  addAtHead(key, val) {
    const node = new HashTableNode(key, val);
    super.addAtHead(node);
  }
}

module.exports = HashTableLinkedList;
