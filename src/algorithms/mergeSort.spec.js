const mergeSort = require('./mergeSort');

describe('Merge Sort', () => {
  it('Returns array untouched if empty array', () => {
    const arr = [];
    assert(mergeSort(arr, 0, 0) === arr);
  });

  it('Returns array untouched if single element array', () => {
    const arr = [1];
    assert(mergeSort(arr, 0, 1) === arr);
  });

  it('Returns array untouched if already sorted', () => {
    const arr = [1, 2, 3, 4, 5];
    assert(mergeSort(arr, 0, 2) === arr);
  });

  it('Sorts array if not already sorted', () => {
    const arr = [5, 2, 3, 4, 1];
    assert(mergeSort(arr, 0, 2) === arr.sort());
  });
});
