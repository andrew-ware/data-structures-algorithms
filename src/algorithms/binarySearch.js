const { isArrayEmpty } = require('../util');

// Perform an efficient search on a [ascending-order] sorted arr of elements of a comparable type
// Complexity:
// Time: O(logn)
// Space: O(1)
const binarySearch = (arr = [], target) => {
  if (!isArrayEmpty(arr) && typeof target !== 'undefined') {
    let left = 0;
    let right = arr.length - 1;
    let mid;
    while (left <= right) {
      // Math.floor here is synonymous with casting to int in typed languages
      mid = Math.floor(left + (right - left) / 2);
      if (arr[mid] === target) {
        return mid;
      }
      if (arr[mid] < target) {
        left = mid + 1;
      } else {
        // synonymous with `else if arr[mid] > target`
        right = mid - 1;
      }
    }
  }
  return -1;
};

module.exports = binarySearch;
