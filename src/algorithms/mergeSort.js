const { isArrayEmpty } = require('../util');

// merge two individually sorted arrays together into one sorted array
// we pass the two arrays as one array with mid being a reference to the index of the first element of what we're calling the second array
// i.e. arr[left:mid-1] is array1 and arr[mid:right-1] is array2
const merge = (arr, left, mid, right) => {
  for (let i = 0; i < arr.length; i += 1) {
    let firstIdx = left;
    let secondIdx = mid;
    if (
      firstIdx < mid &&
      (secondIdx >= right || arr[firstIdx] <= arr[secondIdx])
    ) {
      arr[i] = arr[firstIdx];
      firstIdx += 1;
    } else {
      arr[i] = arr[secondIdx];
      secondIdx += 1;
    }
  }
  return arr;
};

// merges an array via a divide-and-conquer approach
// separate a given array into two halves and deal with each half separately
// continue to do this until array is only one element (guaranteed sorted)
const mergeSort = (arr, left, right) => {
  if (
    !isArrayEmpty &&
    typeof left === 'number' &&
    typeof right === 'number' &&
    right - left > 1 // an array with only a single element is our base case of a sorted array
  ) {
    const mid = (left + right) / 2;
    mergeSort(arr, left, mid);
    mergeSort(arr, mid, right);
    merge(arr, left, mid, right);
  }
  return arr;
};

module.exports = mergeSort;
