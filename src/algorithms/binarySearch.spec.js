const binarySearch = require('./binarySearch');

describe('Binary Search', () => {
  describe('Returns -1 if target does not exist in arr', () => {
    it('If arr empty', () => {
      assert(binarySearch([], 1) === -1);
    });

    it('If target is undefined', () => {
      assert(binarySearch([1, 2, 3]) === -1);
    });

    it('If target not present in arr', () => {
      assert(binarySearch([1, 2, 3], 4) === -1);
    });
  });

  describe('Returns index of target element in array', () => {
    it('If array elements are non-duplicated ints', () => {
      assert(binarySearch([1, 2, 3, 4, 5], 2) === 1);
    });

    it('If array elements are all duplicated ints', () => {
      // it doesn't matter which index it finds so long as it finds one
      assert(binarySearch([1, 1, 1, 1, 1], 1) !== -1);
    });

    it('If some array elements are duplicated ints, but others are unique', () => {
      // it doesn't matter which index it finds so long as it corresponds to a target element
      const idx = binarySearch([1, 1, 2, 3, 4], 1);
      assert(idx >= 0 && idx <= 1);
    });

    it('If elements are strings', () => {
      assert(binarySearch(['apple', 'banana', 'orange', 'pear'], 'pear') === 3);
    });
  });
});
